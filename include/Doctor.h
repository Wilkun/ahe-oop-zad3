//
// Created by wiedymin on 06.05.2019.
//
#include <utility>
#include <string>
#include <list>
#include <iostream>

#ifndef ZADANIE3_DOCTOR_H
#define ZADANIE3_DOCTOR_H

namespace ahe {
    struct Doctor {
        std::string name;
        int age;
        std::list<std::string> specializations;
        std::string work_hours;
        std::string registration_date;

        explicit Doctor(std::string _name = {}, int _age = {},
                        std::list<std::string> _specializations = {}, std::string _work_hours = {},
                        std::string _registration_date = {});

        virtual ~Doctor() {};

        friend std::ostream &operator<<(std::ostream &os, const Doctor &doctor) {
            os << "name: " << doctor.name << ",\tage: " << doctor.age <<  ",\twork_hours: " << doctor.work_hours << ",\tregistration_date: " << doctor.registration_date;
            return os;
        }
    };
}
#endif //ZADANIE3_DOCTOR_H
