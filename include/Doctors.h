//
// Created by wiedymin on 06.05.2019.
//
#include <Doctor.h>
#include <vector>
#include <algorithm>
#include <memory>
#include <stdexcept>

#ifndef ZADANIE3_DOCTORS_H
#define ZADANIE3_DOCTORS_H

namespace ahe {

    struct Doctors {
        std::vector<Doctor> Container;

        void add(std::string _name, int _age, std::list<std::string> _specializations, std::string _work_hours,
                 std::string _registration_date);

        void remove(std::string name);

        auto find(std::string const &needle);

        void print( int index );

        static void print(Doctor &record);

        void print(std::string const &name);

        void print_all();

        void modify(std::string const &_name, std::string _new_name, int _age, std::list<std::string> _specializations, std::string _work_hours,
                    std::string _registration_date);

        void modify( int index, std::string _new_name, int _age, std::list<std::string> _specializations, std::string _work_hours,
                    std::string _registration_date);

        Doctors() = default;

        virtual ~Doctors() {};
    };
}


#endif //ZADANIE3_DOCTORS_H
