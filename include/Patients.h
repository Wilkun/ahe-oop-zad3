//
// Created by wiedymin on 06.05.2019.
//
#include <Patient.h>
#include <vector>
#include <algorithm>
#include <memory>
#include <stdexcept>

#ifndef ZADANIE3_PATIENTS_H
#define ZADANIE3_PATIENTS_H

namespace ahe {

    struct Patients {
        std::vector<Patient> Container;

        void add(std::string _first_name, std::string _last_name, int _age, std::list<std::string> _sickness,
                 std::string _registration_date);

        void remove(std::string _last_name);

        auto find(std::string const &needle);

        void print( int index );

        static void print(Patient &record);

        void print(std::string const &name);

        void print_all();

        void modify(std::string const &_name, std::string _new_first_name, std::string _new_last_name, int _age, std::list<std::string> _sickness,
                    std::string _registration_date);

        void modify( int index, std::string _new_first_name, std::string _new_last_name, int _age, std::list<std::string> _sickness, std::string _registration_date);

        Patients() = default;

        virtual ~Patients() {};
    };
}


#endif //ZADANIE3_PATIENTS_H
