//
// Created by wiedymin on 06.05.2019.
//
#include <utility>
#include <string>
#include <list>
#include <iostream>

#ifndef ZADANIE3_PATIENT_H
#define ZADANIE3_PATIENT_H

namespace ahe {
    struct Patient {
        std::string first_name;
        std::string last_name;
        int age;
        std::list<std::string> sickness;
        std::string registration_date;

        explicit Patient(std::string _first_name = {}, std::string _last_name = {}, int _age = {},
                         std::list<std::string> _sickness = {},
                         std::string _registration_date = {});

        virtual ~Patient() {};

        friend std::ostream &operator<<(std::ostream &os, const Patient &Patient) {
            os << "first name: " << Patient.first_name
                << ",\tlast name: " << Patient.last_name
                << ",\tage: " << Patient.age
                << ",\tregistration_date: " << Patient.registration_date;
            return os;
        }
    };
}
#endif //ZADANIE3_PATIENT_H
