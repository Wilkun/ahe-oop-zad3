//
// Created by wiedymin on 09.05.2019.
//

#include <Doctor.h>

ahe::Doctor::Doctor(std::string _name, int _age, std::list <std::string> _specializations, std::string _work_hours,
                    std::string _registration_date) :
        name(std::move(_name)), age(_age), specializations(std::move(_specializations)),
        work_hours(std::move(_work_hours)),
        registration_date(std::move(_registration_date)) {}


