#include <Doctors.h>
#include <Patients.h>
#include <iostream>
#include <typeinfo>
#include <memory>
#include <cstdio>

int main() {
    //  Wektory pozwalają na dynamiczne alokowanie tablicy o zmiennej wielkości. Dodatkowo Szablon vector umożliwia
    //  dodawanie oraz usuwanie elementów na koniec tablicy w czasie stałym - O(1). Dodawanie oraz usuwanie elementów
    //  z początku bądź ze środka kontenera możliwe jest w czasie liniowym - O(n), gdzie n to liczba elementów
    //  w kontenerze.

    {
        auto doctors_base = std::make_unique<ahe::Doctors>();
        std::cout << "Doctors base:" << std::endl;
        doctors_base->add("Piotr Kowal", 55, {"onkologia"}, "10-12", "2015-05-05");
        doctors_base->add("Adam Nowak", 32, {"ginekologia", "poloznictwo"}, "13-17", "2017-09-13");
        doctors_base->add("Kamil Polak", 28, {"internista"}, "8-10,13-15", "2019-02-24");
        doctors_base->add("Malgorzata Kowalczyk", 44, {"urologia"}, "14-17", "2016-09-13");

        doctors_base->print_all();

        doctors_base->remove("Piotr Kowal");

        doctors_base->print("Piotr Kowal");

        doctors_base->modify("Adam Nowak","Anna Nowak", 32, {"ginekologia", "urologia"}, "13-17", "2017-09-13");

        doctors_base->print("Anna Nowak");

        doctors_base->print_all();
    }
    {
        auto patients_base = std::make_unique<ahe::Patients>();
        std::cout << std::endl << "{Patients base:" << std::endl;
        patients_base->add("Piotr", "Kowal", 55, {"rak"}, "2015-05-05");
        patients_base->add("Adam", "Nowak", 32, {"kaszel", "zlamana noga"}, "2017-09-13");
        patients_base->add("Kamil", "Polak", 28, {"wysypka"}, "2019-02-24");
        patients_base->add("Malgorzata", "Kowalczyk", 44, {"grypa"}, "2016-09-13");

        patients_base->print_all();

        patients_base->remove("Kowal");

        patients_base->print("Kowal");

        patients_base->modify("Nowak", "Anna", "Nowak", 31, {"hipohondria"}, "2017-09-13");

        patients_base->print("Nowak");

        patients_base->print_all();
    }
    return 0;
}