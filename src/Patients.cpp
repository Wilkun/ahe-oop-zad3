//
// Created by wiedymin on 09.05.2019.
//

#include <Patients.h>

void ahe::Patients::add(std::string _first_name, std::string _last_name, int _age, std::list<std::string> _sickness,
                        std::string _registration_date) {
    Patient new_patient{_first_name, _last_name, _age, _sickness, _registration_date};
    Container.push_back(new_patient);
}

void ahe::Patients::remove(std::string _last_name) {
    Container.erase(
            std::remove_if(Container.begin(), Container.end(), [&](Patient const &_patient) {
                return _patient.last_name == _last_name;
            }),
            Container.end());
}

auto ahe::Patients::find(std::string const &needle) {
    auto it = std::find_if(Container.begin(), Container.end(),
                           [&needle](const Patient &d) { return d.last_name == needle; });
    if (it != Container.end()) {
        return std::distance(Container.begin(), it);
    } else {
        throw std::runtime_error("No record found");
    }
}

void ahe::Patients::print(int index) {
    std::cout << Container.at(index) << std::endl;
}

void ahe::Patients::print(ahe::Patient &record) {
    std::cout << record << std::endl;
}

void ahe::Patients::print(std::string const &name) {
    try {
        auto record = this->find(name);
        this->print(record);
    } catch (const std::exception& e) {
        std::cout << e.what()  << std::endl;
    }
}

void ahe::Patients::print_all() {
    std::for_each(Container.begin(), Container.end(), [&](Patient const &_patient) {
        std::cout << _patient << std::endl;
    });
}

void ahe::Patients::modify(std::string const &_name, std::string _new_first_name, std::string _new_last_name, int _age,
                           std::list<std::string> _sickness, std::string _registration_date) {
    try {
        auto record = this->find(_name);
        this->modify(record, _new_first_name, _new_last_name, _age, _sickness, _registration_date );
    } catch (const std::exception& e) {
        std::cout << e.what()  << std::endl;
    }
}

void ahe::Patients::modify(int index, std::string _new_first_name, std::string _new_last_name, int _age,
                           std::list<std::string> _sickness, std::string _registration_date) {
    Container.at(index).first_name = _new_first_name;
    Container.at(index).last_name = _new_last_name;
    Container.at(index).age = _age;
    Container.at(index).sickness = _sickness;
    Container.at(index).registration_date = _registration_date;
}
