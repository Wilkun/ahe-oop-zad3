//
// Created by wiedymin on 09.05.2019.
//

#include <Patient.h>

ahe::Patient::Patient(std::string _first_name, std::string _last_name, int _age, std::list <std::string> _sickness,
                      std::string _registration_date) :
        first_name(std::move(_first_name)), last_name(std::move(_last_name)), age(_age),
        sickness(std::move(_sickness)),
        registration_date(std::move(_registration_date)) {}
