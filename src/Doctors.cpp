//
// Created by wiedymin on 09.05.2019.
//
#include <Doctors.h>

void ahe::Doctors::add(std::string _name, int _age, std::list <std::string> _specializations, std::string _work_hours,
                       std::string _registration_date) {
    Doctor new_doctor{_name, _age, _specializations, _work_hours, _registration_date};
    Container.push_back(new_doctor);
}

void ahe::Doctors::remove(std::string name) {
    Container.erase(
            std::remove_if(Container.begin(), Container.end(), [&](Doctor const &_doctor) {
                return _doctor.name == name;
            }),
            Container.end());
}

auto ahe::Doctors::find(std::string const &needle) {
    auto it = std::find_if(Container.begin(), Container.end(),
                           [&needle](const Doctor &d) { return d.name == needle; });
    if (it != Container.end()) {
        return std::distance(Container.begin(), it);
    } else {
        throw std::runtime_error("No record found");
    }
}

void ahe::Doctors::print(int index) {
    std::cout << Container.at(index) << std::endl;
}

void ahe::Doctors::print(ahe::Doctor &record) {
    std::cout << record << std::endl;
}

void ahe::Doctors::print(std::string const &name) {
    try {
        auto record = this->find(name);
        this->print(record);
    } catch (const std::exception& e) {
        std::cout << e.what()  << std::endl;
    }
}

void ahe::Doctors::print_all() {
    std::for_each(Container.begin(), Container.end(), [&](Doctor const &_doctor) {
        std::cout << _doctor << std::endl;
    });
}

void
ahe::Doctors::modify(std::string const &_name, std::string _new_name, int _age, std::list<std::string> _specializations,
                     std::string _work_hours, std::string _registration_date) {
    try {
        auto record = this->find(_name);
        this->modify(record, _new_name, _age, _specializations, _work_hours, _registration_date );
    } catch (const std::exception& e) {
        std::cout << e.what()  << std::endl;
    }
}

void ahe::Doctors::modify(int index, std::string _new_name, int _age, std::list<std::string> _specializations,
                          std::string _work_hours, std::string _registration_date) {
    Container.at(index).name = _new_name;
    Container.at(index).age = _age;
    Container.at(index).specializations = _specializations;
    Container.at(index).work_hours = _work_hours;
    Container.at(index).registration_date = _registration_date;
}
