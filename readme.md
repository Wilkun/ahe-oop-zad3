### Zadanie 3

Korzystając z poznanego na wcześniejszych wykładach programowania strukturalnego, oraz niektórych zaawansowanych elementów języka C++ napisz program "Kartoteka medyczna", który będzie przechowywał informację o lekarzach i pacjentach, oraz umożliwiał, dodawanie, usuwanie, modyfikację, oraz wyszukanie lekarza lub pacjenta.

Na informację o lekarzu składają się:

* Imię i nazwisko
* wiek
* Lista specjalizacji
* godziny pracy
* Data rejestracji

Na informację o pacjencie składają się.

* Imię
* Nazwisko
* Wiek
* Lista chorób
* Data rejestracji

Program powinien przechowywać informację w pamięci w poznanych kontenerach biblioteki STL. W programie w postaci komentarzy należy uzasadnić dlaczego zdecydowano się na zastosowanie danego kontenera lub algorytmu.